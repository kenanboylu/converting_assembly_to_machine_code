#include <stdio.h>
#include <string.h>
#include <math.h>
int decimal_binary(int n) 
 {  
 int rem, i=1, binary=0;
 while (n!=0)
 {
 rem=n%2; n/=2;
 binary+=rem*i;
 i*=10; 
} 
return binary;
 }

void logisim(){
     FILE *cfPtr3;
     FILE *cfPtr4;
     int a=0;
     char binaryNumber[16];
      if ( ( cfPtr4 = fopen( "binary.txt", "r" ) ) == NULL ){
          printf("none");
      }
      else{
           fscanf( cfPtr4, "%s",binaryNumber);
           
           if ( ( cfPtr3 = fopen( "LogisimOutput.hex", "w" ) ) != NULL )
               fprintf(cfPtr3,"v2.0 raw\n"); 
           while ( !feof( cfPtr4 ) ) { 
         
         
          
   char hexaDecimal[16];
   int temp;
   long int i=0,j=0;

   while(binaryNumber[i]){
      binaryNumber[i] = binaryNumber[i] -48;
      ++i;
   }

   --i;
   while(i-2>=0){
       temp =  binaryNumber[i-3] *8 + binaryNumber[i-2] *4 +  binaryNumber[i-1] *2 + binaryNumber[i] ;
       if(temp > 9)
            hexaDecimal[j++] = temp + 55;
       else
            hexaDecimal[j++] = temp + 48;
       i=i-4;
   }

   if(i ==1)
      hexaDecimal[j] = binaryNumber[i-1] *2 + binaryNumber[i] + 48 ;
   else if(i==0)
      hexaDecimal[j] =  binaryNumber[i] + 48 ;
    else
      --j;

  
   
   while(j>=0){
       if(hexaDecimal[j]>64){
           hexaDecimal[j]=hexaDecimal[j]+32;
       }
      
       fprintf(cfPtr3,"%c",hexaDecimal[j--]);
      
   }
           a++;
           fprintf(cfPtr3," ");
           if(a==8){
              fprintf(cfPtr3,"\n");
           }
            fscanf( cfPtr4, "%s",binaryNumber);
           }
          
       fclose( cfPtr3 );
        
       fclose( cfPtr4 );
       
      } 
}

void machine(){
        FILE *cfPtr3;
     FILE *cfPtr4;
     char binaryNumber[16];
      if ( ( cfPtr4 = fopen( "binary.txt", "r" ) ) == NULL ){
          printf("none");
      }
      else{
           fscanf( cfPtr4, "%s",binaryNumber);
           
           if ( ( cfPtr3 = fopen( "AssemblerOutput.hex", "w" ) ) != NULL )
           while ( !feof( cfPtr4 ) ) { 
          
         
   char hexaDecimal[16];
   int temp;
   long int i=0,j=0;

   while(binaryNumber[i]){
      binaryNumber[i] = binaryNumber[i] -48;
      ++i;
   }

   --i;
   while(i-2>=0){
       temp =  binaryNumber[i-3] *8 + binaryNumber[i-2] *4 +  binaryNumber[i-1] *2 + binaryNumber[i] ;
       if(temp > 9)
            hexaDecimal[j++] = temp + 55;
       else
            hexaDecimal[j++] = temp + 48;
       i=i-4;
   }

   if(i ==1)
      hexaDecimal[j] = binaryNumber[i-1] *2 + binaryNumber[i] + 48 ;
   else if(i==0)
      hexaDecimal[j] =  binaryNumber[i] + 48 ;
    else
      --j;

  
   
   while(j>=0){
      
       fprintf(cfPtr3,"%c",hexaDecimal[j--]);
   }
           fprintf(cfPtr3,"\n");
           //fprintf(cfPtr3,"\n");
           fscanf( cfPtr4, "%s",binaryNumber);
           }
          
       fclose( cfPtr3 );
        
       fclose( cfPtr4 );
       
      }  
}

int main()
{
   char* inst;
   char* name;
   int imm5;
   int imm43;
   char* token1;
   char* token2;
   char* token3;
   char* string;
   
   int num;
   FILE *cfPtr;   
   FILE *cfPtr1;
  
   if ( ( cfPtr = fopen( "input.txt", "r" ) ) == NULL )
      printf( "File could not be opened\n" );
   else {
      
      fscanf( cfPtr, "%s%s", inst,name); 
      
     if ( ( cfPtr1 = fopen( "binary.txt", "w" ) ) != NULL )
       
      while ( !feof( cfPtr ) ) { 
string =strdup(name);
if (string != NULL) {
token1 = strsep(&string, ",");
token2 = strsep(&string, ",");
token3 = strsep(&string, ",");

}
         
          if(strcmp(inst,"LD")==0){
              num=100;
              
            
             fprintf( cfPtr1, "%.4d",num);
            
             token1=strtok(token1, "R");
             num=atoi(token1);
             num=decimal_binary(num);
            
             fprintf( cfPtr1, "%.3d",num); 
             
             token2=strtok(token2, "#");
             num=atoi(token2);
             num=decimal_binary(num);
            
            
             fprintf( cfPtr1, "%.9d",num); 
          }
         
         if(strcmp(inst,"ST")==0){
              num=1100;
              
             
             fprintf( cfPtr1, "%.4d",num);
             
             token1=strtok(token1, "R");
             num=atoi(token1);
             num=decimal_binary(num);
             
             fprintf( cfPtr1, "%.3d",num); 
             
             token2=strtok(token2, "#");
             num=atoi(token2);
             num=decimal_binary(num);
             
            
             fprintf( cfPtr1, "%.9d",num); 
          }
         
          if(strcmp(inst,"JMP")==0){
              num=1010;
              
            
             fprintf( cfPtr1, "%.4d",num);
            
             token1=strtok(token1, "#");
             num=atoi(token1);
             num=decimal_binary(num);
            
             fprintf( cfPtr1, "%.12d",num); 
             
          }
         
         
         if(strcmp(inst,"ADD")==0){
            
             num=1000;
     
            
             fprintf( cfPtr1, "%.4d",num);
             token1=strtok(token1, "R");
             num=atoi(token1);
             num=decimal_binary(num);
           
          
             fprintf( cfPtr1, "%.3d",num); 
             token2=strtok(token2, "R");
             num=atoi(token2);
             num=decimal_binary(num);
          
             
             fprintf( cfPtr1, "%.3d",num); 
    
         
             if(token3[0]=='R'){
             imm5=0;
             imm43=0; 
            
             fprintf( cfPtr1, "%d",imm5);
            
             fprintf( cfPtr1, "%.2d",imm43);
             token3=strtok(token3, "R");
             num=atoi(token3);
            
             num=decimal_binary(num);
             fprintf( cfPtr1, "%.3d",num);
      
             }
     
             else
             {
             imm5=1;
             
           
             fprintf( cfPtr1, "%d",imm5);
             token3=strtok(token3, "#");
             num=atoi(token3);
             
             num=decimal_binary(num);
           
             fprintf( cfPtr1, "%.5d",num);
             }
                            
  
         }
         if(strcmp(inst,"AND")==0){
            
             num=10;
     
            
             fprintf( cfPtr1, "%.4d",num);
             token1=strtok(token1, "R");
             num=atoi(token1);
             num=decimal_binary(num);
            
          
             fprintf( cfPtr1, "%.3d",num); 
             token2=strtok(token2, "R");
             num=atoi(token2);
             num=decimal_binary(num);
           
             
             fprintf( cfPtr1, "%.3d",num); 
    
         
             if(token3[0]=='R'){
             imm5=0;
             imm43=0; 
            
             fprintf( cfPtr1, "%d",imm5);
            
             fprintf( cfPtr1, "%.2d",imm43);
             token3=strtok(token3, "R");
             num=atoi(token3);
            
             num=decimal_binary(num);
            
             fprintf( cfPtr1, "%.3d",num);
      
             }
     
             else
             {
             imm5=1;
             
             fprintf( cfPtr1, "%d",imm5);
             token3=strtok(token3, "#");
             num=atoi(token3);
             
             num=decimal_binary(num);
           
             fprintf( cfPtr1, "%.5d",num);
             }
         }
         
     
          fscanf( cfPtr, "%s%s", inst,name);
          fprintf( cfPtr1, "\n");
      }
       
       fclose( cfPtr1 );  
       fclose( cfPtr );
   }
    logisim();
     
    machine();
    printf("Proceses successfully completed.\n");
    
   return 0;
}
